# sdk-dingtalk ![](https://img.shields.io/maven-central/v/net.guerlab.sdk/sdk-dingtalk.svg)![](https://img.shields.io/badge/LICENSE-LGPL--3.0-brightgreen.svg)
> spring boot下钉钉企业应用的开箱既用环境

## 使用场景
> spring boot应用中需要接入钉钉企业应用

## 开始使用

> 1. pom.xml中引入依赖

```
<dependency>
    <groupId>net.guerlab.sdk</groupId>
    <artifactId>sdk-dingtalk-starter</artifactId>
</dependency>

```

> 2. bootstrap.yml中增加配置

```
sdk:
  ding-talk:
    enable: #是否启用自动配置
    corp-id: #企业ID
    app-key: #appKey
    app-secret: #appSecret
```

> 3. 增加对象注入

```
import net.guerlab.sdk.dingtalk.client.DingTalkClient;

public class Demo {

    @Autowired
    private DingTalkClient dingTalkClient;
}
```

> 4. 调用请求

https://oapi.dingtalk.com/user/getuserinfo 请求范例
```
UserGetUserInfoRequest request = new UserGetUserInfoRequest();
request.setCode("code demo");
UserGetUserInfoResponse response = client.execute(request);
System.out.println(response);
```
