package net.guerlab.sdk.dingtalk.autoconfigure;

import net.guerlab.sdk.dingtalk.autoconfigure.client.DefaultDingTalkClientAutoConfiguration;
import net.guerlab.sdk.dingtalk.autoconfigure.storage.DingTalkMemoryStorageAutoConfiguration;
import net.guerlab.sdk.dingtalk.autoconfigure.storage.DingTalkRedisTemplateStorageAutoConfiguration;
import org.springframework.boot.autoconfigure.ImportAutoConfiguration;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * 钉钉自动配置
 *
 * @author guer
 */
@Configuration
@ConditionalOnProperty(prefix = "sdk.ding-talk", value = "enable", havingValue = "true")
@EnableConfigurationProperties(DingTalkProperties.class)
@ImportAutoConfiguration({ DefaultDingTalkClientAutoConfiguration.class, DingTalkMemoryStorageAutoConfiguration.class,
        DingTalkRedisTemplateStorageAutoConfiguration.class })
public class DingTalkAutoConfiguration {}
