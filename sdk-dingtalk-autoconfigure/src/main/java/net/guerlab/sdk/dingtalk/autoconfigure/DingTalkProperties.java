package net.guerlab.sdk.dingtalk.autoconfigure;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.context.config.annotation.RefreshScope;

/**
 * 钉钉配置
 *
 * @author guer
 */
@Data
@RefreshScope
@ConfigurationProperties(prefix = DingTalkProperties.PREFIX)
public class DingTalkProperties {

    public static final String PREFIX = "sdk.ding-talk";

    /**
     * 是否启用自动配置
     */
    private boolean enable = true;

    /**
     * 企业ID
     */
    private String corpId;

    /**
     * appKey
     */
    private String appKey;

    /**
     * appSecret
     */
    private String appSecret;
}
