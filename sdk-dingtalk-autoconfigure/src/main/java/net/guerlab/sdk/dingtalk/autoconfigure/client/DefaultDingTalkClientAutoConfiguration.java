package net.guerlab.sdk.dingtalk.autoconfigure.client;

import lombok.extern.slf4j.Slf4j;
import net.guerlab.sdk.dingtalk.client.DingTalkClient;
import net.guerlab.sdk.dingtalk.client.impl.DefaultDingTalkClient;
import net.guerlab.sdk.dingtalk.storage.DingTalkConfigStorage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.net.http.HttpClient;

/**
 * 钉钉客户端自动配置
 *
 * @author guer
 */
@SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
@Slf4j
@Configuration
public class DefaultDingTalkClientAutoConfiguration {

    private HttpClient httpClient;

    @Bean
    @RefreshScope
    public DingTalkClient dingTalkOkHttpClient(DingTalkConfigStorage storage) {
        log.debug("init default client");
        DingTalkClient client = new DefaultDingTalkClient(httpClient == null ? HttpClient.newHttpClient() : httpClient);
        client.setDingTalkConfigStorage(storage);
        return client;
    }

    @Autowired(required = false)
    public void setHttpClient(HttpClient httpClient) {
        this.httpClient = httpClient;
    }
}
