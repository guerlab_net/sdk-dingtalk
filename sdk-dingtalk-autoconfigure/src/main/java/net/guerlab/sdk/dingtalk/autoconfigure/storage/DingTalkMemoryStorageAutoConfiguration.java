package net.guerlab.sdk.dingtalk.autoconfigure.storage;

import lombok.extern.slf4j.Slf4j;
import net.guerlab.sdk.dingtalk.autoconfigure.DingTalkProperties;
import net.guerlab.sdk.dingtalk.storage.DingTalkConfigMemoryStorage;
import net.guerlab.sdk.dingtalk.storage.DingTalkConfigStorage;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 钉钉配置储存接口自动配置
 *
 * @author guer
 */
@Slf4j
@Configuration
@ConditionalOnMissingBean(DingTalkConfigStorage.class)
public class DingTalkMemoryStorageAutoConfiguration {

    @Bean
    @RefreshScope
    public DingTalkConfigStorage dingTalkConfigMemoryStorage(DingTalkProperties properties) {
        log.debug("init memory storage");
        DingTalkConfigStorage storage = new DingTalkConfigMemoryStorage();
        storage.setCorpId(properties.getCorpId());
        storage.setAppKey(properties.getAppKey());
        storage.setAppSecret(properties.getAppSecret());
        return storage;
    }
}
