package net.guerlab.sdk.dingtalk;

/**
 * 钉钉常量
 *
 * @author guer
 */
public interface DingTalkConstants {

    /**
     * GET请求
     */
    String METHOD_GET = "GET";

    /**
     * POST请求
     */
    String METHOD_POST = "POST";

    /**
     * PUT请求
     */
    String METHOD_PUT = "PUT";

    /**
     * DELETE请求
     */
    String METHOD_DELETE = "DELETE";

    /**
     * 基础路径
     */
    String BASE_URL = "https://oapi.dingtalk.com/";

    /**
     * JSON请求数据类型
     */
    String CONTENT_TYPE_JSON = "application/json";

}
