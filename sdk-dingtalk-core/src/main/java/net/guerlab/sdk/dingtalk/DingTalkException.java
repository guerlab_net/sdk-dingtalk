package net.guerlab.sdk.dingtalk;

import lombok.Getter;

/**
 * 钉钉异常
 *
 * @author guer
 */
@Getter
public class DingTalkException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    private final int errCode;

    /**
     * 通过异常信息构造异常
     *
     * @param message
     *         异常信息
     * @param errCode
     *         错误码
     */
    public DingTalkException(String message, int errCode) {
        super(message);
        this.errCode = errCode;
    }

    /**
     * 通过异常信息和源异常构造异常
     *
     * @param message
     *         异常信息
     * @param e
     *         源异常
     */
    public DingTalkException(String message, Exception e) {
        super(message, e);
        this.errCode = 0;
    }

    /**
     * 通过异常信息和源异常构造异常
     *
     * @param message
     *         异常信息
     * @param errCode
     *         错误码
     * @param e
     *         源异常
     */
    public DingTalkException(String message, int errCode, Exception e) {
        super(message, e);
        this.errCode = errCode;
    }
}
