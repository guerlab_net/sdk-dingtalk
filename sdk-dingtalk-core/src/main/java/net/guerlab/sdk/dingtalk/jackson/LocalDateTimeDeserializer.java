package net.guerlab.sdk.dingtalk.jackson;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import lombok.extern.slf4j.Slf4j;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

/**
 * @author guer
 */
@Slf4j
public class LocalDateTimeDeserializer extends JsonDeserializer<LocalDateTime> {

    @Override
    public LocalDateTime deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) {
        try {
            long timestamp = Long.parseLong(jsonParser.getValueAsString());
            return LocalDateTime.ofInstant(new Date(timestamp).toInstant(), ZoneId.systemDefault());
        } catch (Exception e) {
            log.debug(e.getLocalizedMessage(), e);
            return null;
        }
    }
}
