package net.guerlab.sdk.dingtalk.request;

import lombok.Getter;
import lombok.Setter;
import net.guerlab.sdk.dingtalk.response.AbstractResponse;

/**
 * 抽象请求
 *
 * @param <T>
 *         响应类型
 * @author guer
 */
@Setter
@Getter
public abstract class AbstractDingTalkRequest<T extends AbstractResponse> extends AbstractRequest<T> {

    /**
     * 请求是否需要AccessToken
     *
     * @return 请求是否需要AccessToken
     */
    public boolean needAccessToken() {return true;}

    /**
     * 请求是否需要Appkey
     *
     * @return 请求是否需要Appkey
     */
    public boolean needAppkey() {return true;}

    /**
     * 请求是否需要AppSecret
     *
     * @return 请求是否需要AppSecret
     */
    public boolean needAppSecret() {return true;}
}
