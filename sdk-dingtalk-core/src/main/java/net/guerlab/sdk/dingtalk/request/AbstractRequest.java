package net.guerlab.sdk.dingtalk.request;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Getter;
import lombok.Setter;
import net.guerlab.sdk.dingtalk.DingTalkConstants;
import net.guerlab.sdk.dingtalk.response.AbstractResponse;
import net.guerlab.sdk.dingtalk.utils.RequestParamsUtils;

import java.util.Map;
import java.util.TreeMap;

/**
 * 抽象请求
 *
 * @param <T>
 *         响应类型
 * @author guer
 */
@Setter
@Getter
public abstract class AbstractRequest<T extends AbstractResponse> {

    protected final Map<String, String> requestParams = new TreeMap<>();

    private ObjectMapper objectMapper;

    /**
     * 获取响应类类型
     *
     * @return 响应类类型
     */
    public abstract Class<T> getResponseClass();

    /**
     * 获取请求方式
     *
     * @return 请求方式
     */
    public String getMethod() {
        return DingTalkConstants.METHOD_POST;
    }

    /**
     * 获取请求正文
     *
     * @return 请求正文
     */
    public String getRequestContent() {
        return null;
    }

    /**
     * 获取基本请求地址
     *
     * @return 请求地址
     */
    public String getRequestUri() {
        return RequestParamsUtils.build(getBaseUrl(), requestParams);
    }

    /**
     * 构造基本请求地址
     *
     * @return 请求地址
     */
    protected abstract String getBaseUrl();
}
