package net.guerlab.sdk.dingtalk.request;

import lombok.Getter;
import lombok.Setter;
import net.guerlab.sdk.dingtalk.response.AbstractResponse;

/**
 * 抽象请求
 *
 * @param <T>
 *         响应类型
 * @author guer
 */
@Setter
@Getter
public abstract class AbstractSnsRequest<T extends AbstractResponse> extends AbstractRequest<T> {}
