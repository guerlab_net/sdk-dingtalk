package net.guerlab.sdk.dingtalk.request;

import lombok.Getter;
import lombok.Setter;
import net.guerlab.sdk.dingtalk.DingTalkConstants;
import net.guerlab.sdk.dingtalk.response.GetTokenResponse;

/**
 * 获取AccessToken请求
 *
 * @author guer
 */
@Setter
@Getter
public class GetTokenRequest extends AbstractDingTalkRequest<GetTokenResponse> {

    /**
     * appkey
     */
    private String appkey;

    /**
     * appsecret
     */
    private String appsecret;

    @Override
    public Class<GetTokenResponse> getResponseClass() {
        return GetTokenResponse.class;
    }

    @Override
    protected String getBaseUrl() {
        return DingTalkConstants.BASE_URL + "gettoken";
    }

    @Override
    public String getMethod() {
        return DingTalkConstants.METHOD_GET;
    }

    @Override
    public boolean needAccessToken() {
        return false;
    }
}
