package net.guerlab.sdk.dingtalk.request.sns;

import net.guerlab.sdk.dingtalk.DingTalkConstants;
import net.guerlab.sdk.dingtalk.request.AbstractSnsRequest;
import net.guerlab.sdk.dingtalk.response.sns.GetUserInfoByCodeResponse;

/**
 * 获取用户信息
 *
 * @author guer
 */
public class GetUserInfoByCodeRequest extends AbstractSnsRequest<GetUserInfoByCodeResponse> {

    /**
     * 临时授权码
     */
    private String tmpAuthCode;

    /**
     * 设置临时授权码
     *
     * @param tmpAuthCode
     *         临时授权码
     */
    public void setTmpAuthCode(String tmpAuthCode) {
        this.tmpAuthCode = tmpAuthCode;
    }

    @Override
    public Class<GetUserInfoByCodeResponse> getResponseClass() {
        return GetUserInfoByCodeResponse.class;
    }

    @Override
    protected String getBaseUrl() {
        return DingTalkConstants.BASE_URL + "sns/getuserinfo_bycode";
    }

    @Override
    public String getMethod() {
        return DingTalkConstants.METHOD_POST;
    }

    @Override
    public String getRequestContent() {
        return "{\"tmp_auth_code\":\"" + tmpAuthCode + "\"}";
    }
}
