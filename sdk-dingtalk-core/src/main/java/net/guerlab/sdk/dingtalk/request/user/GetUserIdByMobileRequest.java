package net.guerlab.sdk.dingtalk.request.user;

import net.guerlab.sdk.dingtalk.DingTalkConstants;
import net.guerlab.sdk.dingtalk.request.AbstractDingTalkRequest;
import net.guerlab.sdk.dingtalk.response.user.GetUserIdByMobileResponse;

/**
 * @author guer
 */
public class GetUserIdByMobileRequest extends AbstractDingTalkRequest<GetUserIdByMobileResponse> {

    /**
     * 设置手机号
     *
     * @param mobile
     *         手机号
     */
    public void setMobile(String mobile) {
        this.requestParams.put("mobile", mobile);
    }

    @Override
    public Class<GetUserIdByMobileResponse> getResponseClass() {
        return GetUserIdByMobileResponse.class;
    }

    @Override
    protected String getBaseUrl() {
        return DingTalkConstants.BASE_URL + "user/get_by_mobile";
    }

    @Override
    public String getMethod() {
        return DingTalkConstants.METHOD_GET;
    }

    @Override
    public boolean needAppkey() {
        return false;
    }

    @Override
    public boolean needAppSecret() {
        return false;
    }
}
