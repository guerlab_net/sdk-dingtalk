package net.guerlab.sdk.dingtalk.request.user;

import net.guerlab.sdk.dingtalk.DingTalkConstants;
import net.guerlab.sdk.dingtalk.request.AbstractDingTalkRequest;
import net.guerlab.sdk.dingtalk.response.user.GetUserIdByUnionIdResponse;

/**
 * @author guer
 */
public class GetUserIdByUnionIdRequest extends AbstractDingTalkRequest<GetUserIdByUnionIdResponse> {

    /**
     * 设置unionId
     *
     * @param unionId
     *         unionId
     */
    public void setUnionId(String unionId) {
        this.requestParams.put("unionid", unionId);
    }

    @Override
    public Class<GetUserIdByUnionIdResponse> getResponseClass() {
        return GetUserIdByUnionIdResponse.class;
    }

    @Override
    protected String getBaseUrl() {
        return DingTalkConstants.BASE_URL + "user/getUseridByUnionid";
    }

    @Override
    public String getMethod() {
        return DingTalkConstants.METHOD_GET;
    }

    @Override
    public boolean needAppkey() {
        return false;
    }

    @Override
    public boolean needAppSecret() {
        return false;
    }
}
