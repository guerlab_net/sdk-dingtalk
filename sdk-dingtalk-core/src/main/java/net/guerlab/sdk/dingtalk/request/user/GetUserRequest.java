package net.guerlab.sdk.dingtalk.request.user;

import net.guerlab.sdk.dingtalk.DingTalkConstants;
import net.guerlab.sdk.dingtalk.request.AbstractDingTalkRequest;
import net.guerlab.sdk.dingtalk.response.user.GetUserResponse;

/**
 * @author guer
 */
public class GetUserRequest extends AbstractDingTalkRequest<GetUserResponse> {

    /**
     * 设置用户ID
     *
     * @param userId
     *         用户ID
     */
    public void setUserId(String userId) {
        this.requestParams.put("userid", userId);
    }

    @Override
    public Class<GetUserResponse> getResponseClass() {
        return GetUserResponse.class;
    }

    @Override
    protected String getBaseUrl() {
        return DingTalkConstants.BASE_URL + "user/get";
    }

    @Override
    public String getMethod() {
        return DingTalkConstants.METHOD_GET;
    }

    @Override
    public boolean needAppkey() {
        return false;
    }

    @Override
    public boolean needAppSecret() {
        return false;
    }
}
