package net.guerlab.sdk.dingtalk.request.user;

import net.guerlab.sdk.dingtalk.DingTalkConstants;
import net.guerlab.sdk.dingtalk.request.AbstractDingTalkRequest;
import net.guerlab.sdk.dingtalk.response.user.UserGetUserInfoResponse;

/**
 * @author guer
 */
public class UserGetUserInfoRequest extends AbstractDingTalkRequest<UserGetUserInfoResponse> {

    /**
     * 设置code
     *
     * @param code
     *         code
     */
    public void setCode(String code) {
        this.requestParams.put("code", code);
    }

    @Override
    public Class<UserGetUserInfoResponse> getResponseClass() {
        return UserGetUserInfoResponse.class;
    }

    @Override
    protected String getBaseUrl() {
        return DingTalkConstants.BASE_URL + "user/getuserinfo";
    }

    @Override
    public String getMethod() {
        return DingTalkConstants.METHOD_GET;
    }

    @Override
    public boolean needAppkey() {
        return false;
    }

    @Override
    public boolean needAppSecret() {
        return false;
    }
}
