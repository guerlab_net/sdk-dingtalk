package net.guerlab.sdk.dingtalk.response;

import lombok.Getter;
import lombok.Setter;

/**
 * 抽象响应对象
 *
 * @author guer
 */
@Setter
@Getter
public abstract class AbstractResponse {

    /**
     * 返回码
     */
    private int errcode;

    /**
     * 对返回码的文本描述内容
     */
    private String errmsg;
}
