package net.guerlab.sdk.dingtalk.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * 获取AccessToken响应
 *
 * @author guer
 */
@Setter
@Getter
public class GetTokenResponse extends AbstractResponse {

    /**
     * AccessToken
     */
    @JsonProperty("access_token")
    private String accessToken;

    /**
     * 过期时间
     */
    @JsonProperty("expires_in")
    private Long expiresIn;
}
