package net.guerlab.sdk.dingtalk.response.sns;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import net.guerlab.sdk.dingtalk.response.AbstractResponse;

/**
 * @author guer
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class GetUserInfoByCodeResponse extends AbstractResponse {

    @JsonProperty("user_info")
    private UserInfo userInfo;

    @Data
    public static class UserInfo {

        private String nick;

        @JsonProperty("openid")
        private String openId;

        @JsonProperty("unionid")
        private String unionId;
    }
}
