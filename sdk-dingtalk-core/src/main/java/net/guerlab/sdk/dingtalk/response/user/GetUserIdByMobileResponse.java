package net.guerlab.sdk.dingtalk.response.user;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import net.guerlab.sdk.dingtalk.response.AbstractResponse;

/**
 * @author guer
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class GetUserIdByMobileResponse extends AbstractResponse {

    @JsonProperty("userid")
    private String userId;

}
