package net.guerlab.sdk.dingtalk.response.user;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import net.guerlab.sdk.dingtalk.response.AbstractResponse;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

/**
 * 用户详情
 *
 * @author guer
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class GetUserResponse extends AbstractResponse {

    /**
     * 员工在当前企业内的唯一标识，也称staffId。可由企业在创建时指定，并代表一定含义比如工号，创建后不可修改
     */
    @JsonProperty("userid")
    private String userId;

    /**
     * 员工在当前开发者企业账号范围内的唯一标识，系统生成，固定值，不会改变
     */
    @JsonProperty("unionid")
    private String unionId;

    /**
     * 员工名字
     */
    private String name;

    /**
     * 分机号（仅限企业内部开发调用）
     */
    private String tel;

    /**
     * 办公地点
     */
    private String workPlace;

    /**
     * 备注
     */
    private String remark;

    /**
     * 手机号码
     */
    private String mobile;

    /**
     * 员工的电子邮箱
     */
    private String email;

    /**
     * 员工的企业邮箱，如果员工已经开通了企业邮箱，接口会返回，否则不会返回
     */
    private String orgEmail;

    /**
     * 是否已经激活
     */
    private Boolean active;

    /**
     * 在对应的部门中的排序，Map结构的json字符串，key是部门的id，value是人员在这个部门的排序值
     */
    private String orderInDepts;

    /**
     * 是否为企业的管理员
     */
    private Boolean isAdmin;

    /**
     * 是否为企业的老板
     */
    private Boolean isBoss;

    /**
     * 在对应的部门中是否为主管：Map结构的json字符串，key是部门的id，value是人员在这个部门中是否为主管，true表示是，false表示不是
     */
    private String isLeaderInDepts;

    /**
     * 是否号码隐藏
     */
    private Boolean isHide;

    /**
     * 成员所属部门id列表
     */
    private List<Long> department;

    /**
     * 职位信息
     */
    private String position;

    /**
     * 头像url
     */
    private String avatar;

    /**
     * 入职时间
     */
    private LocalDateTime hiredDate;

    /**
     * 员工工号
     */
    @JsonProperty("jobnumber")
    private String jobNumber;

    /**
     * 扩展属性
     */
    @JsonProperty("extattr")
    private Map<String, String> extAttr;

    /**
     * 是否是高管
     */
    private String isSenior;

    /**
     * 国家地区码
     */
    private String stateCode;

    /**
     * 用户所在角色列表
     */
    private List<Role> roles;

    /**
     * 是否实名认证
     */
    private Boolean realAuthed;

    @Data
    public static class Role {

        /**
         * 角色id
         */
        private Long id;

        /**
         * 角色名称
         */
        private String name;

        /**
         * 角色组名称
         */
        private String groupName;
    }
}
