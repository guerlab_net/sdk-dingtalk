package net.guerlab.sdk.dingtalk.storage;

import java.util.concurrent.locks.Lock;

/**
 * 钉钉配置储存接口
 *
 * @author guer
 */
public interface DingTalkConfigStorage {

    /**
     * 获取AccessToken
     *
     * @return AccessToken
     */
    String getAccessToken();

    /**
     * 获取AccessToken锁
     *
     * @return AccessToken锁
     */
    Lock getAccessTokenLock();

    /**
     * 获取AccessToken过期状态
     *
     * @return AccessToken是否过期
     */
    boolean isAccessTokenExpired();

    /**
     * 强制将access token过期
     */
    void expireAccessToken();

    /**
     * 更新AccessToken
     *
     * @param accessToken
     *         新的accessToken值
     * @param expiresInSeconds
     *         过期时间，以秒为单位
     */
    void updateAccessToken(String accessToken, long expiresInSeconds);

    /**
     * 获取企业ID
     *
     * @return 企业ID
     */
    String getCorpId();

    /**
     * 设置企业ID
     *
     * @param corpId
     *         企业ID
     */
    void setCorpId(String corpId);

    /**
     * 获取appKey
     *
     * @return appKey
     */
    String getAppKey();

    /**
     * 设置appKey
     *
     * @param appKey
     *         appKey
     */
    void setAppKey(String appKey);

    /**
     * 获取appSecret
     *
     * @return appSecret
     */
    String getAppSecret();

    /**
     * 设置appSecret
     *
     * @param appSecret
     *         appSecret
     */
    void setAppSecret(String appSecret);
}
